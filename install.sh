#!/usr/bin/env bash

# install nokvm web controller script 
# create by:  gustack
# time：2019-05-13 15:42:42

stty erase ^h
stty erase ^?
YUM_INSTALL="yum install -y"
RPM_INSTALL="rpm -Uvh"
WWW_ROOT="/home/wwwroot"
VERSION="3.0.1"
NGINX_CONFIG="/etc/nginx/conf.d/default.conf"
NGINX_DEFAULT_CONF="/etc/nginx/nginx.conf"
NGINX_CONFIG_DIR="/etc/nginx/conf.d"
NGINX_DEFAULT_DIR="/etc/nginx"
NOKVM_CONTROLLER="gustack_controller_${VERSION}.tar.gz"
CONTROLLER_DIR="control"
FAKESERVER_DIR="gustackcn"
NGINX_CONFIG_FILE="gustack.conf"
NGINX_FAKESERVER_CONFIG_FILE="gustackcn.conf"
CRON_FILE="/var/spool/cron/root.cron"
DOWNLOAD_URL="http://down.gustack.cn/soft"
SG_DIR="/usr/lib64/php/modules"
SG_FILE="ixed.7.1.lin"

clear
echo -e "\033[32m +------------------------------------------------------------------------+ \033[0m"
echo -e  "\033[32m |          Welcome to install GuStackIDC财务系统 V${VERSION}                    | \033[0m"
echo -e "\033[32m +------------------------------------------------------------------------+ \033[0m"
echo -e "\033[32m |                       官网：www.gustack.cn                              | \033[0m"
echo -e "\033[32m +------------------------------------------------------------------------+ \033[0m"
echo -e "\033[32m |               客服QQ：59378001   QQ群：985577853                        | \033[0m"
echo -e "\033[32m +------------------------------------------------------------------------+ \033[0m"
echo -e "\033[32m  \033[0m"
echo -e "\033[32m 破解版本 v0.1 \033[0m"
echo -e "\033[32m 域名变更前请先修改 /home/wwwroot/gustackcn 目录下 index.php \033[0m"
echo -e "\033[32m 然后删除 /home/wwwroot/control/runtime 文件夹下全部内容 \033[0m"
echo -e "\033[32m 即可正常使用 \033[0m"

sleep 2

# 初始化目录
echo -e "\033[32m Initial Directory... \033[0m"
if [ ! -d $WWW_ROOT ];then
    mkdir ${WWW_ROOT}  -p  >> /dev/null 2>&1
    setenforce  0  >> /dev/null 2>&1
    sed -i 's/SELINUX=enforcing/SELINUX=disabled/g' /etc/selinux/config >> /dev/null 2>&1
fi

sleep 1

# 安装lnmp
echo -e "\033[32m Install LNMP Service Please Wait... \033[0m"
yum install -y epel-release
yum install -y yum-axelget
yum -y localinstall http://nginx.org/packages/centos/7/noarch/RPMS/nginx-release-centos-7-0.el7.ngx.noarch.rpm
rpm -Uvh https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
rpm -Uvh https://mirror.webtatic.com/yum/el7/webtatic-release.rpm
yum -y install wget ntpdate  supervisor php71w php71w-fpm php71w-mysqlnd php71w-mbstring php71w-common php71w-gd php71w-mcrypt php71w-xml php71w-cli php71w-devel nginx mariadb mariadb-server
echo -e "\033[32m Install LNMP Success... \033[0m"

sleep 1

# 设置时区同步

mv -f /etc/localtime /etc/localtime.bak >> /dev/null 2>&1
ln -s /usr/share/zoneinfo/Asia/Shanghai  /etc/localtime >> /dev/null 2>&1

if [ -f ${CRON_FILE} ];then
    rm -rf ${CRON_FILE}
fi
echo "*/2 * * * * root /usr/sbin/ntpdate 0.asia.pool.ntp.org" >> ${CRON_FILE}
echo "*/1 * * * *  php /home/wwwroot/control/think Crons >> /dev/null 2>&1" >> ${CRON_FILE}
crontab ${CRON_FILE} >> /dev/null 2>&1
systemctl enable crond >> /dev/null 2>&1
systemctl restart crond >> /dev/null 2>&1

# 配置nginx
groupadd www >> /dev/null 2>&1
useradd -g www -s /sbin/nologin www >> /dev/null 2>&1

if [ -f ${NGINX_DEFAULT_CONF} ];then
    rm -rf ${NGINX_DEFAULT_CONF}
fi

if [ -f ${NGINX_CONFIG} ];then
    rm -rf ${NGINX_CONFIG}
fi

wget -O ${NGINX_DEFAULT_CONF}  ${DOWNLOAD_URL}/nginx.conf >> /dev/null 2>&1
wget -O ${NGINX_CONFIG_DIR}/${NGINX_CONFIG_FILE}  ${DOWNLOAD_URL}/${NGINX_CONFIG_FILE} >> /dev/null 2>&1
# 配置伪站conf
cp -ar  ${NGINX_FAKESERVER_CONFIG_FILE}  ${NGINX_CONFIG_DIR}
service nginx restart

systemctl  start nginx >> /dev/null 2>&1
systemctl  enable nginx >> /dev/null 2>&1
echo -e "\033[32m Running NGINX Success... \033[0m"

sleep 1

# 配置mysql
systemctl  start mariadb >> /dev/null 2>&1
systemctl  enable mariadb >> /dev/null 2>&1

while true
do
    read -p "Please set MySQL password: " MYSQL_PASS
    if [ ! ${MYSQL_PASS} ]; then
        echo -e "\033[31m input value not null \033[0m"
    else
        mysql -uroot -e "update mysql.user set password=password('"${MYSQL_PASS}"') where user='root';flush privileges";
        break
    fi    
done

mysql -uroot -p${MYSQL_PASS} -e "create database control"


echo -e "\033[32m Running MYSQL Success... \033[0m"

sleep 1

# 配置supervisord

mkdir /etc/supervisor/conf.d -p >> /dev/null 2>&1

echo_supervisord_conf > /etc/supervisord.conf 

cat >> /etc/supervisord.conf << EOF
[include]
files = /etc/supervisor/conf.d/*.conf
EOF

echo "supervisord  -c /etc/supervisord.conf" >> /etc/rc.d/rc.local

supervisord  -c /etc/supervisord.conf >> /dev/null 2>&1

# 配置php
if [ -f ${SG_DIR}/${SG_FILE} ];then
    rm -rf ${SG_DIR}/${SG_FILE} >> /dev/null 2>&1
fi

wget -O ${SG_DIR}/${SG_FILE} ${DOWNLOAD_URL}/${SG_FILE} >> /dev/null 2>&1

sed -i 's/user = apache/user = www/g'  /etc/php-fpm.d/www.conf >> /dev/null 2>&1
sed -i 's/group = apache/group = www/g' /etc/php-fpm.d/www.conf >> /dev/null 2>&1
sed -i '$a\extension=ixed.7.1.lin'  /etc/php.ini >> /dev/null 2>&1
systemctl  start php-fpm >> /dev/null 2>&1
systemctl  enable php-fpm >> /dev/null 2>&1
echo -e "\033[32m Running PHP-FPM Success... \033[0m"
sleep 1

# 配置伪站
echo -e "\033[32m Make Controller Fake Server Please Wait... \033[0m"
cp -ar  ${FAKESERVER_DIR}  ${WWW_ROOT}
chown www:www ${WWW_ROOT}/${FAKESERVER_DIR} -R
chmod -R 766 ${WWW_ROOT}/${FAKESERVER_DIR}
echo "127.0.0.1 www.gustack.cn" >> /etc/hosts
echo -e "\033[32m 请注意:域名开头无需填写 http:// 或 https:// \033[0m"
echo -e "\033[32m 授权的域名区分子域名 \033[0m"
echo -e "\033[32m 如 www.gustack.cn 和 demo.gustack.cn 视为不同域名 \033[0m"
echo -e "\033[32m \033[0m"
echo -e "\033[32m 如果授权域名填写错误 \033[0m"
echo -e "\033[32m 请先修改 /home/wwwroot/gustackcn 目录下 index.php \033[0m"
echo -e "\033[32m 然后删除 /home/wwwroot/control/runtime 文件夹下全部内容 \033[0m"

while true
do
    read -p "授权域名: " Domain
    if [ ! ${Domain} ]; then
        echo -e "\033[31m input value not null \033[0m"
    else
        sed -i "s/$url = '';/$url = '${Domain}';/g" ${WWW_ROOT}/${FAKESERVER_DIR}/index.php;
        break
    fi    
done

# 配置controller
echo -e "\033[32m Make Controller Web Project Please Wait... \033[0m"
#cp composer /usr/local/bin/
cp -ar  ${CONTROLLER_DIR}  ${WWW_ROOT}
chown www:www ${WWW_ROOT}/${CONTROLLER_DIR} -R
chmod -R 766 ${WWW_ROOT}/${CONTROLLER_DIR}
cd ${WWW_ROOT}/${CONTROLLER_DIR}
cp .env.example .env
sed -i  "s/database=homestead/database=${CONTROLLER_DIR}/g" .env
sed -i "s/username=homestead/username=root/g" .env
sed -i "s/password=secret/password=${MYSQL_PASS}/g" .env
firewall-cmd --zone=public --add-port=80/tcp --permanent >> /dev/null 2>&1
firewall-cmd --zone=public --add-port=6080/tcp --permanent >> /dev/null 2>&1
firewall-cmd --reload >> /dev/null 2>&1
#composer install >> /dev/null 2>&1
cd ${WWW_ROOT}/${CONTROLLER_DIR} 
#php artisan  key:generate >> /dev/null 2>&1
#php artisan  command:install

rm -rf ${WWW_ROOT}/${CONTROLLER_DIR}/runtime/*
chmod g+s ${WWW_ROOT}/${CONTROLLER_DIR}/runtime/
setfacl -R -m u:www:rwx  ${WWW_ROOT}/${CONTROLLER_DIR}/runtime/

sleep 1

systemctl  restart php-fpm >> /dev/null 2>&1

while true
do
    read -p "授权码: " Serial_Numbe
    if [ ! ${Serial_Numbe} ]; then
        echo -e "\033[31m input value not null \033[0m"
    else
        sed -i "s/app_key=key/app_key=${Serial_Numbe}/g" .env;
        break
    fi    
done
cd ${WWW_ROOT}/${CONTROLLER_DIR}
mysql -uroot -hlocalhost -p${MYSQL_PASS} ${CONTROLLER_DIR}< install.sql
echo -e  "\033[33m ---------------------------------------------------- \033[0m"
echo -e "\033[33m 管理地址：http://domain  \033[0m"
echo -e "\033[33m 默认账号: admin \033[0m"
echo -e "\033[33m 默认密码: admin888 \033[0m"
echo -e "\033[33m ---------------------------------------------------- \033[0m"
rm -rf install.sql
chmod -R 777 /var/lib/php/session/
rm -rf ${WWW_ROOT}/${CONTROLLER_DIR}/runtime/*