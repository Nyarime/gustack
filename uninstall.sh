#!/usr/bin/bash

# uninstall nokvm web controller script 
# create by:  Tablib
# time：2019-05-13 15:42:42

echo -e "\033[32m Stop LNMP Services... \033[0m"
systemctl stop nginx >> /dev/null 2>&1
systemctl stop php-fpm >> /dev/null 2>&1
systemctl stop mariadb >> /dev/null 2>&1
supervisorctl  stop novnc >> /dev/null 2>&1
ps -ef | grep supervisord | grep -v grep | awk '{print $2}' | xargs kill -9 >> /dev/null  2>&1
rm -rf /etc/supervisor/conf.d/novnc.conf >> /dev/null 2>&1
userdel -r www >> /dev/null 2>&1
rm -rf /home/wwwroot/control >> /dev/null 2>&1
rm -rf /home/wwwroot/gustackcn >> /dev/null 2>&1
rm -rf /usr/lib64/php/modules/ixed.7.1.lin >> /dev/null 2>&1
crontab  -r >> /dev/null 2>&1
systemctl  restart crond
rm -rf  /var/lib/mysql/
rm -rf /etc/nginx/
rm -rf /etc/php-fpm.d/
rm -rf /etc/my.cnf
rm -rf /etc/supervisord.conf
rm -rf /etc/supervisor/

sleep 1

echo -e "\033[32m Uninstall LNMP... \033[0m"
yum -y  remove ntpdate  supervisor php71w php71w-fpm php71w-mysqlnd php71w-mbstring php71w-common php71w-gd php71w-mcrypt php71w-xml php71w-cli php71w-devel nginx mariadb mariadb-server >> /dev/null 2>&1
echo -e "\033[32m Uninstall Success done... \033[0m"
